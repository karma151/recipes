<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Recipe;

class IngredientController extends Controller
{

  /**
  * Matches /ingredient/*
  *
  * @Route("/ingredient/{slug}")
  */
  public function showAction($slug)
  {
    $repository = $this->getDoctrine()->getRepository(Ingredient::class);
    $ingredient = $repository->findOneBySlug($slug);

    if (!$ingredient){
      throw $this->createNotFoundException('L\'ingredient n\'a pas été trouvé');
    }
    $em = $this->getDoctrine()->getManager();    

    $recipes = $em->createQueryBuilder()
                ->select('recipe')
                ->from(Recipe::class, 'recipe')
                ->leftJoin('recipe.parts', 'part')
                ->leftJoin('part.ingredients', 'parhasingredient')
                ->leftJoin('parhasingredient.ingredient', 'ingredient')
                ->andWhere('ingredient.id = ' . $ingredient->getId())
                ->getQuery()->execute();

      return $this->render('ingredient/show.html.twig', array(
      "ingredient" => $ingredient,
      "recipes" => $recipes
    ));
  }

  /**
  * @Route("/search", name="recipe_search")
  */
  public function searchAction(Request $request)
  {
    $form = $this->createForm('AppBundle\Form\RecipeType', null);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->get('ingredients')->getData();

      $em = $this->getDoctrine()->getManager();
      $recipes = $em->getRepository('AppBundle:Recipe')->findByIngredients($data);
      return $this->render('recipe/search_result.html.twig', array('recipes' => $recipes));
    }

    $ingredients = $this->getDoctrine()->getRepository('AppBundle:Ingredient')->findAll();

    return $this->render('recipe/search.html.twig', array('form' => $form->createView(), 'ingredients' => $ingredients));
  }

  /**
  * @Route("/search/autocomplete", name="recipe_search_autocomplete")
  */
  public function searchAutocompleteAction(Request $request)
  {
    $data = array();
    $term = trim(strip_tags($request->get('term')));

    $em = $this->getDoctrine()->getManager();

    $recipes = $em->getRepository('AppBundle:Recipe')->createQueryBuilder('c')
    ->where('c.name LIKE :name')
    ->setParameter('name', '%'.$term.'%')
    ->setMaxResults(3)
    ->getQuery()
    ->getResult();

    $data["recipes"] = array();
    foreach ($recipes as $recipe)
    {
      $data["recipes"][] = array(
        "name" => $recipe->getName(),
         "id" => $recipe->getId(),
         "slug" => $recipe->getSlug(),
         "image" => $this->container->get('sonata.media.twig.extension')->path($recipe->getMedia(), 'reference')
       );
    }

    $ingredients = $em->getRepository('AppBundle:Ingredient')->createQueryBuilder('i')
    ->where('i.name LIKE :name')
    ->setParameter('name', '%'.$term.'%')
    ->setMaxResults(5)
    ->getQuery()
    ->getResult();

    $data["ingredients"] = array();
    foreach ($ingredients as $ingredient)
    {
      $data["ingredients"][] = array(
        "name" => $ingredient->getName(),
         "id" => $ingredient->getId()
       );
    }

    $response = new JsonResponse();
    $response->setData($data);

    return $response;
  }
}
