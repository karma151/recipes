<?php
namespace AppBundle\Handler;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use MaxBeckers\AmazonAlexa\RequestHandler\AbstractRequestHandler;
use MaxBeckers\AmazonAlexa\Helper\ResponseHelper;
use MaxBeckers\AmazonAlexa\Request\Request\Standard\LaunchRequest;
use MaxBeckers\AmazonAlexa\Request\Request as Request;
use MaxBeckers\AmazonAlexa\Response\Response;

class LaunchRequestHandler extends AbstractRequestHandler
{
  public function __construct()
  {
    $this->log("[__construct]");
    $this->supportedApplicationIds = ['amzn1.ask.skill.ef3e5741-dead-4948-8bfb-00803ee21379'];
  }

  public function supportsRequest(Request $request): bool
  {
      $this->log("[supportsRequest]");
      if ($request->request instanceOf LaunchRequest)
      {
        $this->log("LaunchRequest supported");
        return true;
      }
      return false;
  }

  public function handleRequest(Request $request): Response
  {
      $this->log("[handleRequest]");
      $response = new ResponseHelper();
      return $response->respond("Bienvenu sur Beat Eat, quelle recette puis-je vous trouver ? Vous pouvez me demander de vous trouvr une recette aléatoire");
  }

  private function log($data){
    $date = new \DateTime();
    file_put_contents("/var/log/testHandler.log", "[" . $date->format('Y-m-d H:i:s') . "] " . $data, FILE_APPEND | LOCK_EX);
  }
}
