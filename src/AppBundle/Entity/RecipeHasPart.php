<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 */
class RecipeHasPart
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Recipe", inversedBy="parts",cascade={"all"})
    * @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
    */
    private $recipe;

    /**
    * @ORM\Column(type="string", length=255, nullable=false)
    */
    private $name;

    /**
    * @var ArrayCollection Ingredients
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\PartHasIngredient", mappedBy="part",cascade={"all"},orphanRemoval=true )
    */
    private $ingredients;

    /**
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return RecipeHasPart
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set recipe.
     *
     * @param \AppBundle\Entity\Recipe|null $recipe
     *
     * @return RecipeHasPart
     */
    public function setRecipe(\AppBundle\Entity\Recipe $recipe = null)
    {
        $this->recipe = $recipe;

        return $this;
    }

    /**
     * Get recipe.
     *
     * @return \AppBundle\Entity\Recipe|null
     */
    public function getRecipe()
    {
        return $this->recipe;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return RecipeHasPart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add ingredient.
     *
     * @param \AppBundle\Entity\PartHasIngredient $ingredient
     *
     * @return RecipeHasPart
     */
    public function addIngredient(\AppBundle\Entity\PartHasIngredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
        $ingredient->setPart($this);

        return $this;
    }

    /**
     * Remove ingredient.
     *
     * @param \AppBundle\Entity\PartHasIngredient $ingredient
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIngredient(\AppBundle\Entity\PartHasIngredient $ingredient)
    {
        return $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}
