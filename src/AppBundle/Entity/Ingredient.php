<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* @ORM\Entity
*/
class Ingredient
{
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @ORM\Column(type="string", length=255, unique=false)
  * @Assert\NotBlank()
  */
  private $name;

  /**
  * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Unit", cascade={"persist"},cascade={"all"})
  */
  private $defaultUnit;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isVegan = false;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isVegetarian = false;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isGlutenfree = false;

  /**
  * @var ArrayCollection Unit
  * @ORM\OneToMany(targetEntity="AppBundle\Entity\IngredientHasUnit", mappedBy="ingredient",cascade={"all"},orphanRemoval=true )
  */
  private $units;

  /**
  * @ORM\Column(type="text", unique=false, nullable=true)
  */
  private $description;

  /**
  * @ORM\Column(type="text", unique=false, nullable=true)
  */
  private $advice;

  /**
  * @Gedmo\Slug(fields={"name"})
  * @ORM\Column(length=128, unique=true)
  */
  private $slug;

  // Constructor
  public function __construct()
  {
    $this->units = new ArrayCollection();
  }

  /**
  * Print for admin
  *
  * @return string
  */
  public function __toString()
  {
    return $this->getName();
  }

  /**
  * Get id
  *
  * @return integer
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set name
  *
  * @param string $name
  *
  * @return Ingredient
  */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
  * Get name
  *
  * @return string
  */
  public function getName()
  {
    return $this->name;
  }

  /**
  * Add unit
  *
  * @param \AppBundle\Entity\IngredientHasUnit $unit
  *
  * @return Ingredient
  */
  public function addUnit(\AppBundle\Entity\IngredientHasUnit $unit)
  {
    $unit->setIngredient($this);
    $this->units[] = $unit;

    return $this;
  }

  /**
  * Remove unit
  *
  * @param \AppBundle\Entity\IngredientHasUnit $unit
  */
  public function removeUnit(\AppBundle\Entity\IngredientHasUnit $unit)
  {
    $this->units->removeElement($unit);
  }

  /**
  * Get units
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getUnits()
  {
    return $this->units;
  }

  /**
  * Set defaultUnit
  *
  * @param \AppBundle\Entity\Unit $defaultUnit
  *
  * @return Ingredient
  */
  public function setDefaultUnit(\AppBundle\Entity\Unit $defaultUnit = null)
  {
    $this->defaultUnit = $defaultUnit;

    return $this;
  }

  /**
  * Get defaultUnit
  *
  * @return \AppBundle\Entity\Unit
  */
  public function getDefaultUnit()
  {
    return $this->defaultUnit;
  }

  /**
  * Set isVegan.
  *
  * @param bool $isVegan
  *
  * @return Ingredient
  */
  public function setIsVegan($isVegan)
  {
    $this->isVegan = $isVegan;

    return $this;
  }

  /**
  * Get isVegan.
  *
  * @return bool
  */
  public function getIsVegan()
  {
    return $this->isVegan;
  }

  /**
  * Set isVegetarian.
  *
  * @param bool $isVegetarian
  *
  * @return Ingredient
  */
  public function setIsVegetarian($isVegetarian)
  {
    $this->isVegetarian = $isVegetarian;

    return $this;
  }

  /**
  * Get isVegetarian.
  *
  * @return bool
  */
  public function getIsVegetarian()
  {
    return $this->isVegetarian;
  }

  /**
  * Set isGlutenfree.
  *
  * @param bool $isGlutenfree
  *
  * @return Ingredient
  */
  public function setIsGlutenfree($isGlutenfree)
  {
    $this->isGlutenfree = $isGlutenfree;

    return $this;
  }

  /**
  * Get isGlutenfree.
  *
  * @return bool
  */
  public function getIsGlutenfree()
  {
    return $this->isGlutenfree;
  }

  /**
  * Set description.
  *
  * @param string $description
  *
  * @return Ingredient
  */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
  * Get description.
  *
  * @return string
  */
  public function getDescription()
  {
    return $this->description;
  }

  /**
  * Set advice.
  *
  * @param string $advice
  *
  * @return Ingredient
  */
  public function setAdvice($advice)
  {
    $this->advice = $advice;

    return $this;
  }

  /**
  * Get advice.
  *
  * @return string
  */
  public function getAdvice()
  {
    return $this->advice;
  }

  /**
  * Get slug.
  *
  * @return string
  */
  public function getSlug()
  {
    return $this->slug;
  }


    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Ingredient
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
