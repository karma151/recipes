<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use AppBundle\Entity\User;
use AppBundle\Entity\Recipe;
use AppBundle\Entity\UserHasHistory;

class UserHasHistoryRepository extends EntityRepository
{
  public function saveHistory(User $user, Recipe $recipe){
    $em = $this->getEntityManager();

    $history = $this->findOneBy(array(
      "user" => $user,
      "recipe" => $recipe
    ));

    if ($history) {
      $history->setCreated(new \Datetime());
    } else {
      $history = new UserHasHistory();
      $history->setRecipe($recipe);
      $history->setUser($user);
    }
    $em->persist($history);
    $em->flush();
  }
}
