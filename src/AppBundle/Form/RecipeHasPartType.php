<?php
namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\RecipeHasPart;

class RecipeHasPartType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', TextType::class, array(
      'required' => true
    ))
    ->add('ingredients', CollectionType::class, [
      'entry_type'   => PartHasIngredientType::class,
      'label'        => false,
      'allow_add'    => true,
      'allow_delete' => true,
      'prototype'    => true,
      'required'     => false,
      'by_reference' => false,
      'constraints' => array(new Valid()),
      'attr'         => [
        'class' => "ingredients-collection",
      ],
    ])
    ->add('position', HiddenType::class, array(
      'attr' => array(
        'class' => 'part-position'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => RecipeHasPart::class,
      'cascade_validation' => true,
    ));
  }

  public function getBlockPrefix()
  {
    return 'RecipeHasPartType';
  }
}
