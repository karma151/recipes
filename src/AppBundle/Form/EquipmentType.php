<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Equipment;

class EquipmentType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', null, array(
      'label' => "Nom de l'equipement"
    ))
    ->add('submit', SubmitType::class, array(
      'label' => "Enregistrer",
      'attr' => array(
        'class' => 'btn'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Equipment::class,
    ));
  }

  public function getBlockPrefix()
  {
    return 'EquipmentType';
  }
}
