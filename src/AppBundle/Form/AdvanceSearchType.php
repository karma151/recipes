<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Form\AdvanceSearchIngredientType;
use AppBundle\Entity\Category;
use AppBundle\Entity\Equipment;


class AdvanceSearchType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', TextType::class, array(
      'label' => "Titre",
      'required' => false,
      'attr' => array(
        'placeholder' => 'Le nom comporte ...'
      )
    ))
    ->add('only', CheckboxType::class, array(
      'required' => false,
    ))
    ->add('categories', EntityType::class, array(
      'class' => Category::class,
      'required' => false,
      'expanded' => true,
      'multiple' => true
    ))
    ->add('equipments', EntityType::class, array(
      'class' => Equipment::class,
      'required' => false,
      'expanded' => true,
      'multiple' => true
    ))
    ->add('ingredients', CollectionType::class, array(
      'entry_type' => AdvanceSearchIngredientType::class,
      'entry_options' => array(
        'label' => false
      ),
      'allow_add' => true,
    ))
    ->add('submit', SubmitType::class, array(
      'label' => 'Rechercher'
    ))
    ;
  }
}
