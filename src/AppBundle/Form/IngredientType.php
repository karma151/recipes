<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\RecipeHasIngredientType;
use AppBundle\Form\StepType;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Step;

class IngredientType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', null, array(
      'label' => "Nom de l'ingrédient"
    ))
    ->add('description', null, array(
      'label' => "Description de l'ingrédient",
      'attr' => array(
        'class' => 'materialize-textarea'
      )
    ))
    ->add('advice', null, array(
      'label' => "Conseils sur l'ingrédient",
      'attr' => array(
        'class' => 'materialize-textarea'
      )
    ))
    ->add('isVegan', null, array(
      'label' => "Ingrédient Vegan ?"
    ))
    ->add('isVegetarian', null, array(
      'label' => "Ingrédient Végétarien"
    ))
    ->add('isGlutenfree', null, array(
      'label' => "Ingrédient sans gluten ?"
    ))
    ->add('submit', SubmitType::class, array(
      'label' => "Enregistrer",
      'attr' => array(
        'class' => 'btn'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Ingredient::class,
    ));
  }

  public function getBlockPrefix()
  {
    return 'IngredientType';
  }
}
