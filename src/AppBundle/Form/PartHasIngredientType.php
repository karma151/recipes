<?php

namespace AppBundle\Form;

use AppBundle\Entity\PartHasIngredient;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PartHasIngredientType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('ingredient')
    ->add('unit')
    ->add('value', null, array(
      'attr' => array(
        'placeholder' => 'Nombre d\'ingrédient'
      )
    )
    )
    ->add('position', HiddenType::class, array(
      'attr' => array(
        'class' => 'ingredient-position'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => PartHasIngredient::class,
      'cascade_validation' => true,
    ));
  }

  public function getBlockPrefix()
  {
    return 'PartHasIngredientType';
  }
}
