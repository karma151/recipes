<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\RecipeHasIngredientType;
use AppBundle\Form\StepType;
use AppBundle\Entity\Timer;

class TimerType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', null, array(
      'label' => "Titre"
    ))
    ->add('time', null, array(
      'label' => "Temps"
    ))
    ->add('position', HiddenType::class, array(
      'attr' => array(
        'class' => 'timer-position'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Timer::class,
    ));
  }

  public function getBlockPrefix()
  {
    return 'TimerType';
  }
}
