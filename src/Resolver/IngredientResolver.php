<?php
namespace Resolver;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\PartHasIngredient;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class IngredientResolver implements ResolverInterface
{
  /**
  * @var EntityManagerInterface
  */
  private $entityManager;

  public function __construct(EntityManagerInterface $entityManagerInterface)
  {
    $this->entityManager = $entityManagerInterface;
  }

  public function resolveIngredient(PartHasIngredient $recipe_has_part)
  {
    // Not use anymore
    return [$recipe_has_part->getIngredient()];
  }

}
