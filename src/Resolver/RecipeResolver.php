<?php
namespace Resolver;

use AppBundle\Entity\Recipe;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class RecipeResolver implements ResolverInterface
{
  /**
  * @var EntityManagerInterface
  */
  private $entityManager;

  public function __construct(EntityManagerInterface $entityManagerInterface)
  {
    $this->entityManager = $entityManagerInterface;
  }

  public function resolveRecipe($id, $name)
  {
    if (!$id && !$name) {
      return $this->entityManager->getRepository(Recipe::class)->findAll();
    }
    
    $result = $this->entityManager->getRepository(Recipe::class)->createQueryBuilder('r')
    ->where('r.id = :id')
    ->andWhere('r.name LIKE :name')
    ->setParameter('id', $id)
    ->setParameter('name', '%' . $name . '%')
    ->getQuery()
    ->getResult();

    return $result;
  }

}
