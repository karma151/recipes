<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        if ($client->getResponse()->getStatusCode() == 500) {
          die($client->getResponse()->getContent());
        }
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
