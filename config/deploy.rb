set :repo_url, "https://gitlab.com/karma151/recipes"

# Symfony console commands will use this environment for execution
set :symfony_env,  "prod"

# Set this to 2 for the old directory structure
set :symfony_directory_structure, 3
# Set this to 4 if using the older SensioDistributionBundle
set :sensio_distribution_version, 5

# symfony-standard edition directories
set :app_path, "app"
set :web_path, "web"
set :var_path, "var"
set :bin_path, "bin"

# The next 3 settings are lazily evaluated from the above values, so take care
# when modifying them
set :app_config_path, "app/config"
set :log_path, "var/logs"
set :cache_path, "var/cache"

set :symfony_console_path, "bin/console"
set :symfony_console_flags, "--no-debug"

set :controllers_to_clear, ["config.php"]

# asset management
set :assets_install_path, "web"
set :assets_install_flags,  '--symlink'

# Share files/directories between releases
set :linked_files, %w{app/config/parameters.yml}
set :linked_dirs, %w{var/logs web/uploads app/sessions}

# Set correct permissions between releases, this is turned off by default
# set :file_permissions_paths, ["var/sessions", "var/logs", "var/cache", "web/uploads"]
# set :file_permissions_users, ["apache"]
# set :file_permissions_roles, :all
# set :file_permissions_groups, []
# set :file_permissions_chmod_mode, "0777"
# before "deploy:finished", "deploy:set_permissions:chown"

after :deploy, "deploy:cache_logs_chmod"

namespace :deploy do
    desc "Chmod on app/cache and app/logs dirs"
    task :cache_logs_chmod do
        on roles(:web) do
            ask(:password, nil, echo: false)
            # -S allows to pass password from standard input
            execute "echo #{fetch(:password)} | sudo -S chmod -R 777 #{current_path}/var/logs"
            execute "echo #{fetch(:password)} | sudo -S chmod -R 777 #{current_path}/var/cache"
            execute "echo #{fetch(:password)} | sudo -S chmod -R 777 #{current_path}/var/sessions"
        end
    end
end

# Role filtering
set :symfony_roles, :all
set :symfony_deploy_roles, :all
